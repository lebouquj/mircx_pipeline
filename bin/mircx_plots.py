#! /usr/bin/env python                                                          
# -*- coding: iso-8859-15 -*-

import argparse, subprocess, os, glob, socket, datetime
from mircx_pipeline import log, lookup, mailfile, headers, files, summarise
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits as pyfits
import smtplib
try:
    from email.mime.multipart import MIMEMultipart
except ModuleNotFoundError:
    from email.MIMEMultipart import MIMEMultipart
try:
    from email.mime.text import MIMEText
except ModuleNotFoundError:
    from email.MIMEText import MIMEText
try:
    from email.mime.base import MIMEBase
except ModuleNotFoundError:
    from email.MIMEBase import MIMEBase
from email import encoders
import mirc_bot as slack

class cd:
    """
    Context manager for changing the current working directory
    """
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)
    
    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)
    
    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

#####################################################
# Description of script and parsable options
description = \
"""
description use #1:
 Wrapper for mircx_reduce.py, mircx_calibrate.py,
 mircx_report.py and mircx_transmission.py. 
 
description use #2:
 Wrapper for mircx_reduce.py to explore different
 values of ncoherent and their effect on vis SNR 
 and T3PHI error.
"""

epilog = \
"""
examples use #1:
 mircx_redcal_wrap.py --dates=2018Oct29,2018Oct28 
  --ncoherent=5,10 --ncs=1,1 --nbs=4,4 --snr-threshold=2.0,2.0

NB: length of ncoherent, ncs, nbs, snr-threshold must be
 equal.
 
examples use #2:
 mircx_redcal_wrap.py --dates=2018Oct25 --ncoh-plots=TRUE 
 --email=observer@chara.observatory
"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,
                       formatter_class=argparse.RawDescriptionHelpFormatter,add_help=True)

TrueFalseDefault = ['TRUE','FALSE','TRUEd']
TrueFalse = ['TRUE','FALSE']
TrueFalseOverwrite = ['TRUE','FALSE','OVERWRITE']

parser.add_argument("--raw-dir",dest="raw_dir",default='/data/CHARADATA/MIRCX',type=str,
            help="directory base for the raw data paths [%(default)s]")

parser.add_argument("--red-dir",dest="red_dir",default='/data/MIRCX/reduced',type=str,
            help="directory base for the reduced data paths [%(default)s]")

parser.add_argument("--dates",dest="dates",type=str,
            help="comma-separated list of observation dates to be reduced [%(default)s]")

preproc = parser.add_argument_group ('(1) preproc',
         '\nSet of options used to control the book-keeping'
         ' as well as the preproc and rts reduction steps.')

preproc.add_argument("--reduce",dest="reduce",default='TRUE',
            choices=TrueFalseOverwrite,
            help="(re)do the reduction process [%(default)s]")

preproc.add_argument("--ncs",dest="ncs",type=str,default='1d', 
            help="list of number of frame-offset for cross-spectrum [%(default)s]")

preproc.add_argument("--nbs",dest="nbs",type=str,default='4d', 
            help="list of number of frame-offset for bi-spectrum [%(default)s]")

preproc.add_argument ("--bbias", dest="bbias",type=str,default='TRUEd',
            help="list of bools (compute the BBIAS_COEFF product [%(default)s]?)")

preproc.add_argument("--max-integration-time-preproc", dest="max_integration_time_preproc",
            default='30.d',type=str,
            help='maximum integration into a single file, in (s).\n'
            'This apply to PREPROC, and RTS steps [%(default)s]')

oifits = parser.add_argument_group ('(2) oifits',
         '\nSet of options used to control the oifits\n'
         ' reduction steps.')

oifits.add_argument("--ncoherent",dest="ncoherent",type=str,default='10d', 
            help="list of number of frames for coherent integration [%(default)s]")

oifits.add_argument("--snr-threshold",dest="snr_threshold",type=str,default='2.0d', 
            help="list of SNR threshold for fringe selection [%(default)s]")

oifits.add_argument("--max-integration-time-oifits", dest="max_integration_time_oifits",
            default='150.d',type=str,
            help='maximum integration into a single file, in (s).\n'
            'This apply to OIFITS steps [%(default)s]')

calib = parser.add_argument_group ('(3) calibrate',
        '\nSet of options used to control the calibration steps.')

calib.add_argument("--calibrate",dest="calibrate",default='TRUE',
            choices=TrueFalseOverwrite,
            help="(re)do the calibration process [%(default)s]")

calib.add_argument("--targ-list",dest="targ_list",default='mircx_targets.list',type=str,
            help="local database to query to identify SCI and CAL targets [%(default)s]")

calib.add_argument("--calib-cal",dest="calibCal",default='FALSE',
            choices=TrueFalse, help="calibrate the calibrators? [%(default)s]")

summary = parser.add_argument_group ('(4) summary',
        '\nSet of options used to control the summary report\n'
        'file production and email alerts.')

summary.add_argument("--email",dest="email",type=str,default='', 
            help='email address to send summary report file TO [%(default)s]')

summary.add_argument("--sender",dest="sender",type=str,default='mircx.mystic@gmail.com',
            help='email address to send summary report file FROM [%(default)s]')

compare = parser.add_argument_group ('(5) compare',
        '\nOptions used to control the exploration of the impact'
        'of varying ncoherent on the vis SNR and T3ERR.')

compare.add_argument("--ncoh-plots", dest="ncoh_plots",default='FALSE',
            choices=TrueFalse,
            help="use the wrapper to produce plots of ncoherent vs\n"
            "vis SNR and T3ERR [%(default)s].")

# Parse arguments:
argopt = parser.parse_args ()

# Verbose:
elog = log.trace('mircx_redcal_wrapper')


summarise.plotUV('.')