#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

from astropy.io import fits as pyfits
import matplotlib.pyplot as plt
from datetime import date
import sys, os, argparse, subprocess, glob
from mircx_pipeline import mailfile, log
import numpy as np

import smtplib
from email import encoders
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase

# To-Do:
# 1. Add object name to title of each fiber map
#    (fixed 2020-06-24 CLD)
# 2. Include 'seeing' circle on each fiber map
#    (fixed 2020-06-26 CLD)
# 3. Avoid re-plotting fiber maps which have not
#    been re-done (fixed 2020-06-24 CLD)
# 4. Replace [h] instances with [ht] in LaTeX 
#    (fixed 2020-06-24: CLD)
# 5. Include \clearpage instance(s) into LaTeX
#    (fixed 2020-06-24 (CLD)
# 6. skip the plotting of aborted fiber maps.
#    (fixed 2020-06-25 CLD)


class cd:
    """
    Context manager for changing the current working directory
    """
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

#####################################################
# Description of script and parsable options
description = \
"""
description: create nightly summary plots of fibermaps
"""

epilog = \
"""
example use: python3 mircx_fibermap_report.py 
             --raw-dir=/data3/STAGING/2020Jun/2020Jun08 
             --red-dir=/data6/reduced/2020Jun/2020Jun08
"""

parser = argparse.ArgumentParser(description=description,epilog=epilog,
                       formatter_class=argparse.RawDescriptionHelpFormatter,add_help=True)
parser.add_argument("--raw-dir",dest="rawDir",default='/data/CHARADATA/MIRCX',type=str,
            help="directory base for the raw data paths [%(default)s]")
parser.add_argument("--red-dir",dest="redDir",default='/data/MIRCX/reduced',type=str,
            help="directory base for the reduced data paths [%(default)s]")

# Parse arguments:
argopt = parser.parse_args()

def annotateFMap(hdu,ax,axpos,axlab):
    # add annotation under panel with X0, y0 pos and max flux
    x0 = hdu[m].header['HIERARCH FIBEXP X0']
    y0 = hdu[m].header['HIERARCH FIBEXP Y0']
    ax.annotate('X0,Y0=('+str(int(x0))+','+str(int(y0))+')', (0,1))
    ax.annotate('Fmax ='+str(int(np.max(hdu[m].data))), (0,0))
    # add x and y axis ticks ranging from -size/2 to size/2 with 
    # size = STEPSIZE * number of steps
    gSize = hdu[m].header['HIERARCH FIBEXP STEPSIZE'] * hdu[m].header['HIERARCH FIBEXP NSTEP']
    axlab[0] = str(int(0.0-(gSize/2)+(hdu[m].header['HIERARCH FIBEXP STEPSIZE']/2)))
    axlab[-1] = str(int((gSize/2)-(hdu[m].header['HIERARCH FIBEXP STEPSIZE']/2)))
    axlab[int(len(axpos)/2)] = '0'

def abortCheck(hdu,prevmap):
    # Assume that if the final row in all maps are all zeros,
    # the map has been aborted. 
    d1 = [np.ndarray.tolist(h.data[-1]) for h in hdu[1:]]
    if list(set([item for sublist in d1 for item in sublist])) == [0.0]:
        return 1
    elif prevmap != None:
        # Assume that if all of the array values in this map 
        # are the same as the previous map, the map has been
        # aborted:
        d2 = [np.ndarray.tolist(h.data[-1]) for h in prevmap[1:]]
        # (using i for item; sL for sublist to save space)
        if [i for sL in d1 for i in sL] == [i for sL in d2 for i in sL]:
            return 1
        else:
            return 0
    else:
        return 0

def seeingCirc(ax, fwhm, pSize_um, axpos, color):
    """
    Overlay a seeing circle to the fiber map.
    This relies on the definition of the image 
    scale being 6 microns = 1m chara telescope
    diffraction limit = 1.22*(1.6e-6)*(1/(1.0*206265)).
    
    - fwhm is the seeing in arcsec
    """
    x0 = axpos[int(len(axpos)/2)] # x position of the circle's centre
    y0 = axpos[int(len(axpos)/2)] # y position of the circle's centre
    asec_per_um  = (206265*1.22*1.6e-6)/6. # provides number of arcsec per micron
    pSize_asec = pSize_um * asec_per_um
    r = (fwhm/2.) / pSize_asec
    sCirc1 = plt.Circle((x0, y0), r, color=color, fill=False)
    ax.add_artist(sCirc1)
    log.info('Added seeing circle of '+str(fwhm)+' arcsec to plot')

# make reduced data directory if it doesn't already exist:
subprocess.call('mkdir -p '+argopt.redDir, shell=True)

fMaps = sorted(glob.glob(argopt.rawDir+'/*_fibexpmap.fits.fz'))
# e.g. 'mircx_2020-06-08T00-07-51_fibexpmap.fits.fz'

if len(fMaps) == 0:
    fMaps = sorted(glob.glob(argopt.rawDir+'/*_fibexpmap.fits'))
    if len(fMaps) == 0:
        log.error('No fiber map fits files found!')
        log.info('Skipping fiber map summary.')
        sys.exit()

with pyfits.open(fMaps[0]) as hdu_0:
    # use the shape of the first saved maps to initialise a dummy "previous map"
    prevmap = [np.zeros([len(h.data),len(h.data)]) for h in hdu_0[1:]]

# make the plots
for f in range(0, len(fMaps)):
    hdu = pyfits.open(fMaps[f])
    # Has the fiber map been aborted? (if so, skip it):
    if f == 0:
        pltMap = abortCheck(hdu,None) # will return 1 if 1st map of the night was aborted
    else:
        prevFMap = pyfits.open(fMaps[f-1])
        pltMap = abortCheck(hdu,prevFMap) # will return 1 if maps were aborted
    
    if pltMap == 0:
        log.info('Plotting fiber map '+fMaps[f])
        # Collect the time of observation & object name (if it exists) from the header:
        try:
            t_uts = hdu[0].header['DATE-OBS'].split('T')[1]+'  '+hdu[0].header['OBJECT']
            t_uts = t_uts.replace('NOSTAR', 'STS')
        except KeyError:
            t_uts = hdu[0].header['DATE-OBS'].split('T')[1]
        obsD = hdu[0].header['DATE-OBS'].split('T')[0]
        # Set up figure:
        fig = plt.figure(0, figsize=(10,3))
        fig.suptitle(t_uts)
        # Iterate through beams; add fiber map image to each subplot
        for m in range(1, 7):
            ax = plt.subplot2grid((3,6), (0,m-1), rowspan=2)
            ax.yaxis.set_visible(False)
            axpos = np.arange(0, hdu[m].header['HIERARCH FIBEXP NSTEP'], 1)
            axlab = ['']*len(axpos)
            ax2 = plt.subplot2grid((3,6), (2,m-1))
            ax2.imshow(np.zeros([2,int(hdu[m].header['HIERARCH FIBEXP NSTEP'])]), cmap='gray_r')
            ax2.set_axis_off()
            extHdr = 'MAP'+str(m)
            # Check if the shape of the fiber map array is the same as the previous...
            if hdu[m].data.shape != prevmap[m-1].shape:
                ax.imshow(hdu[m].data, cmap='gray', aspect='equal')
                annotateFMap(hdu,ax2,axpos,axlab)
                seeingCirc(ax, 0.3, hdu[m].header['HIERARCH FIBEXP STEPSIZE'], axpos, 'c')
                seeingCirc(ax, 0.5, hdu[m].header['HIERARCH FIBEXP STEPSIZE'], axpos, 'y')
                seeingCirc(ax, 1.0, hdu[m].header['HIERARCH FIBEXP STEPSIZE'], axpos, 'm')
            else:
                # ... and if it is, ensure we're not just plotting the same map again:
                if np.all(hdu[m].data == prevmap[m-1]):
                    ax.imshow(np.zeros([len(hdu[m].data),len(hdu[m].data)]), cmap='gray_r', aspect='equal')
                    log.info('Beam '+str(m)+' not re-done; skipped.')
                else:
                    ax.imshow(hdu[m].data, cmap='gray', aspect='equal')
                    annotateFMap(hdu,ax2,axpos,axlab)
                    seeingCirc(ax, 0.3, hdu[m].header['HIERARCH FIBEXP STEPSIZE'], axpos, 'c')
                    seeingCirc(ax, 0.5, hdu[m].header['HIERARCH FIBEXP STEPSIZE'], axpos, 'y')
                    seeingCirc(ax, 1.0, hdu[m].header['HIERARCH FIBEXP STEPSIZE'], axpos, 'm')
            ax.grid(None)
            ax.set_xticks(axpos)
            ax.set_xticklabels(axlab)
        try:
            outPltFs.append(argopt.redDir+'/'+fMaps[f].split('/')[-1].split('.')[0]+'.pdf')
        except:
            outPltFs = [argopt.redDir+'/'+fMaps[f].split('/')[-1].split('.')[0]+'.pdf']
        plt.tight_layout()
        fig.savefig(argopt.redDir+'/'+fMaps[f].split('/')[-1].split('.')[0]+'.pdf')
    
    # update the "previous map" details:
    prevmap = [h.data for h in hdu[1:]]

# Write the latex file:
outTexF = argopt.redDir+'/fiberexpmap.tex'
with open(outTexF, 'w') as outtex:
    outtex.write('\\documentclass[a4paper]{article}\n\n')
    outtex.write('\\usepackage{fullpage}\n\\usepackage{amsmath}\n')
    outtex.write('\\usepackage{hyperref}\n\\usepackage{graphicx}\n')
    outtex.write('\\usepackage{longtable}\n')
    outtex.write('\\usepackage[left=1.5cm,right=1.5cm,top=2cm,bottom=2cm]')
    outtex.write('{geometry}\n\n')
    outtex.write('\\begin{document}\n\n')
    outtex.write('Fiber maps from '+obsD+'; seeing circles of 0.3" (cyan), 0.5" (yellow)')
    outtex.write(' and 1.0" (magenta) overlaid.')
    for ou in range(0, len(outPltFs)):
        outtex.write('\\begin{figure}[ht]\n    \\raggedright\n')
        outtex.write('    \\centering\n')
        outtex.write('    \\includegraphics[width=\\textwidth]{'+outPltFs[ou]+'}\n')
        outtex.write('\\end{figure}\n\n')
        if (ou+1) % 3 == 0:
            if ou == 0:
                outtex.write('\n')
            else:
                outtex.write('\\clearpage % '+str(ou)+'\n\n')
    outtex.write('\\end{document}\n')

# Build the pdf file in the reduced data directory
with cd(argopt.redDir):
    subprocess.call('pdflatex '+outTexF, shell=True)

# Email the pdf file:
msg = MIMEMultipart()
msg['From'] = 'mircx.mystic@gmail.com'
msg['To']   = 'mircx-status@exeter.ac.uk'
msg['Subject'] = 'Fiber maps from '+obsD
body = 'Fiber maps from '+obsD
msg.attach(MIMEText(body, 'plain'))
attachment = open(outTexF.replace('.tex', '.pdf'), 'rb')
part = MIMEBase('application', 'octet-stream')
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header('Content-Disposition',"attachment; filename= %s" % outTexF.split('/')[-1].replace('.tex', '.pdf'))
msg.attach(part)
try:
    mailfile.send_email(msg, msg['From'], msg['To'])
except smtplib.SMTPAuthenticationError:
    sys.exit()
