#!/usr/bin/env python3

import glob
import numpy as np
import pandas as pd
from astropy.io import fits

def main():
    #########
    # Read in the headers into a massive dictionary

    def read_header(filename, index=0):
        with fits.open(filename) as file:
            hdr = file[index].header
        return hdr

    defkeys = set()
    hdrs = {}
    hdrs["filename"] = sorted(glob.glob("mircx[0-9]*.fits"))
    hi = 0
    if len(hdrs["filename"]) == 0:
        hdrs["filename"] = sorted(glob.glob("mircx[0-9]*.fits.fz"))
        hi = 1

    for (i, fn) in enumerate(hdrs["filename"]):
        hdr = read_header(fn, index=hi)
        filekeys = set(hdr.keys())
        for k in filekeys.difference(defkeys):
            hdrs[k] = [None for j in range(i)]
        for k in filekeys:
            hdrs[k].append(hdr[k])
        for k in defkeys.difference(filekeys):
            hdrs[k].append(None)
        defkeys = defkeys.union(filekeys)

    hdrs["pop"] = []
    for i in range(len(hdrs["filename"])):
        try:
            hdrs["pop"].append(
                str(hdrs["CHARA S1_POP"][i]) +
                str(hdrs["CHARA S2_POP"][i]) +
                str(hdrs["CHARA E1_POP"][i]) +
                str(hdrs["CHARA E2_POP"][i]) +
                str(hdrs["CHARA W1_POP"][i]) +
                str(hdrs["CHARA W2_POP"][i])
            )
        except KeyError:
            hdrs["pop"].append("unknown")

    #########
    # Keep sky data files at a 10 minute cadence

    cadence = 600 # seconds

    mintime = min(hdrs["TIME_S"])
    mintime += (cadence - mintime % cadence)
    maxtime = max(hdrs["TIME_S"])
    maxtime += (cadence - maxtime % cadence) # range object does not include last value

    keeprows = [np.argmin(abs(np.array(hdrs["TIME_S"]) - time)) for time in range(mintime, maxtime, cadence)]
    keeprows = np.array(keeprows)[sorted(np.unique(keeprows, return_index=True)[1])]
    keeprows = [i for i in keeprows if hdrs["OBJECT"][i] != "" and hdrs["OBJECT"][i] != "STS" and hdrs["FILETYPE"][i] == "DATA"]

    #########
    # Extract sub-"table"

    importantkeys = (
        [
            "UTC-OBS",
            "CONF_NA",
            "pop",
            # "RA",
            # "DEC",
            "HA",
            "REF_TEL"
        ] + 
        ["DLOFFST" + str(i) for i in range(6)] +
        ["ACTBAS" + str(i) for i in range(15)]
    )

    st = {}
    for k in importantkeys:
        st[k] = [hdrs[k][r] for r in keeprows]

    tel = ("S1", "S2", "E1", "E2", "W1", "W2")
    for telname in tel:
        st[telname] = []

    baseline = np.reshape([-1,0,1,2,3,4,0,-1,5,6,7,8,1,5,-1,9,10,11,2,6,9,-1,12,13,3,7,10,12,-1,14,4,8,11,13,14,-1], (6,6))

    note = ("*", "†", "‡", "§", "‖", "¶")
    for (i, reftel) in enumerate(st["REF_TEL"]):
        for (t, telname) in enumerate(tel):
            out = "%.3f" % (st["DLOFFST" + str(t)][i] * 1e3) # stored as m in hdr, but mm in gdt
            # Check to see if offset is valid
            if t == reftel:
                pass
            elif st["ACTBAS" + str(baseline[t, reftel])]:
                pass
            else:
                app = ""
                for tt in range(6):
                    if tt != t and st["ACTBAS" + str(baseline[t, tt])]:
                        app += note[tt]
                if len(app) == 0:
                    out = ""
                else:
                    out += app
            st[telname].append(out)

    #########
    # Make output table

    outputkeys = [
        "UTC-OBS",
        "CONF_NA",
        "pop",
        # "RA",
        # "DEC",
        "HA",
    ] + list(tel)

    df = {}
    for k in outputkeys:
        df[k.upper()] = st[k]
    df = pd.DataFrame(df)
    for k in ["UTC-OBS", "HA"]:
        df[k] = [s[:s.find('.')] for s in df[k]]

    ret_note = "\n"
    for i in range(6):
        ret_note += note[i] + " = cross-fringe with " + tel[i] + "\n"

    return df, ret_note

if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.INFO)
    logging.info("This may take a couple minutes to crunch...")
    df, note = main()
    print()
    print(df.to_string(index=False))
    print(note)
